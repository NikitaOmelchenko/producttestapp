//
//  ProductRequests.swift
//  ProductTestApp
//
//  Created by Nikita Omelchenko on 6/6/19.
//  Copyright © 2019 Nikita Omelchenko. All rights reserved.
//

import UIKit
import PKHUD
import Alamofire

struct ProductRequests {
    
    // MARK: - Get Product List
    
    public func getProductList (_ success: @escaping ([ProductModel]) -> Void) {
        let url = BaseUrl.datahost.rawValue + RequestURL.getProductList.rawValue
        let header = ApiManager.header
        
        self.getList(withUrl: url, header: header, success: { productListResponse in
            success(productListResponse)
        })
    }
    
    // MARK: - Get Product List request
    
    private func getList(withUrl url: String, header: HTTPHeaders!, success: @escaping ([ProductModel]) -> Void) {
        HUD.show(.systemActivity)
        ApiManager.sessionManager.request(url, headers: header).responseData { (response) in
            HUD.hide(animated: true)
            response.result.ifFailure {
                ApiManager.errorProcessing(with: response.response, error: response.result.error)
            }
            
            response.result.ifSuccess {
                let productListResponse: Array<ProductModel> = try! JSONDecoder().decode([ProductModel].self, from: response.result.value!)
                success(productListResponse)
            }
        }
    }
}
