//
//  SignResponse.swift
//  ProductTestApp
//
//  Created by Nikita Omelchenko on 6/6/19.
//  Copyright © 2019 Nikita Omelchenko. All rights reserved.
//

import UIKit
import Alamofire

struct SuccessRespose {
    public func signInResponse(withResponse response: DataResponse<Data>) -> Bool {
        let signResponse: SignModel = try! JSONDecoder().decode(SignModel.self, from: response.result.value!)
        guard signResponse.success == true else {
            ApiManager.errorProcessing(withMsg: signResponse.message)
            return false
        }
        if (signResponse.token != nil) {
            ApiManager.userToken = signResponse.token
        }
        
        return true
    }
}
