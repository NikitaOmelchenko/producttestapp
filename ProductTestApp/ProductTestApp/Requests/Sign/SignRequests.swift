//
//  SignRequests.swift
//  ProductTestApp
//
//  Created by Nikita Omelchenko on 6/6/19.
//  Copyright © 2019 Nikita Omelchenko. All rights reserved.
//

import UIKit
import PKHUD
import Alamofire

struct SignRequests {
    
    // MARK: - Sign Up
    
    public func signUp(withUser user: UserModel!, success: @escaping () -> Void) {
        let url = BaseUrl.datahost.rawValue + RequestURL.registrationUser.rawValue
        let params = try? user.asDictionary()
        
        HUD.show(.systemActivity)
        self.sign(withUrl: url, params: params, success: {
            HUD.hide()
            success()
        }) {
            HUD.hide()
        }
    }
    
    // MARK: - Sign In
    
    public func signIn(withUser user: UserModel!, success: @escaping () -> Void) {
        let url = BaseUrl.datahost.rawValue + RequestURL.login.rawValue
        let params = try? user.asDictionary()
        
        HUD.show(.systemActivity)
        self.sign(withUrl: url, params: params, success: {
            HUD.hide()
            success()
        }) {
            HUD.hide()
        }
    }
    
    // MARK: - Sign request
    
    private func sign(withUrl url: String, params: Parameters!, success: @escaping () -> Void, failure: @escaping () -> Void) {
        ApiManager.sessionManager.request(url, method: .post, parameters: params, encoding: URLEncoding.default).responseData { (response) in
            response.result.ifFailure {
                failure()
                ApiManager.errorProcessing(with: response.response, error: response.result.error)
            }
            
            response.result.ifSuccess {
                guard SuccessRespose().signInResponse(withResponse: response) else {
                    failure()
                    return
                }
                success()
            }
        }
    }
}
