//
//  ReviewRequests.swift
//  ProductTestApp
//
//  Created by Nikita Omelchenko on 6/6/19.
//  Copyright © 2019 Nikita Omelchenko. All rights reserved.
//

import UIKit
import PKHUD
import Alamofire

struct ReviewRequests {
    
    // MARK: - Get Review List
    
    public func getReviewList (withProduct product: ProductModel, success: @escaping ([ReviewModel]) -> Void) {
        let url = BaseUrl.datahost.rawValue + RequestURL.review.rawValue + "\(String(describing: product.id ?? 0))"
        let header = ApiManager.header
        
        self.getList(withUrl: url, header: header, success: { reviewListResponse in
            success(reviewListResponse)
        })
    }
    
    // MARK: - Create Review
    
    public func createReview (withReview reviewModel: ReviewModel, success: @escaping () -> Void, failure: @escaping () -> Void) {
        let url = BaseUrl.datahost.rawValue + RequestURL.review.rawValue + "\(String(describing: reviewModel.product ?? 0))"
        let header = ApiManager.header
        let params = try? reviewModel.asDictionary()
        
        self.create(withUrl: url, params: params, header: header, success: {
            success()
        }) {
            failure()
        }
    }
    
    // MARK: - Get Review List request
    
    private func getList(withUrl url: String, header: HTTPHeaders!, success: @escaping ([ReviewModel]) -> Void) {
        HUD.show(.systemActivity)
        ApiManager.sessionManager.request(url, headers: header).responseData { (response) in
            HUD.hide(animated: true)
            response.result.ifFailure {
                ApiManager.errorProcessing(with: response.response, error: response.result.error)
            }
            
            response.result.ifSuccess {
                let reviewListResponse: Array<ReviewModel> = try! JSONDecoder().decode([ReviewModel].self, from: response.result.value!)
                success(reviewListResponse)
            }
        }
    }
    
    // MARK: - Create Review request
    
    private func create(withUrl url: String, params: Parameters!, header: HTTPHeaders!, success: @escaping () -> Void, failure: @escaping () -> Void) {
        HUD.show(.systemActivity)
        ApiManager.sessionManager.request(url, method: .post, parameters: params, encoding: URLEncoding.default, headers: header).responseData { (response) in
            HUD.hide(animated: true)
            response.result.ifFailure {
                failure()
                ApiManager.errorProcessing(with: response.response, error: response.result.error)
            }
            
            response.result.ifSuccess {
                guard SuccessRespose().signInResponse(withResponse: response) else {
                    failure()
                    return
                }
                success()
            }
        }
    }
}

