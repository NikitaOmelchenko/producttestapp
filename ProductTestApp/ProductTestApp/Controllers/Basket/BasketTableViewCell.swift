//
//  BasketTableViewCell.swift
//  ProductTestApp
//
//  Created by Nikita Omelchenko on 6/11/19.
//  Copyright © 2019 Nikita Omelchenko. All rights reserved.
//

import UIKit

class BasketTableViewCell: UITableViewCell {
    
    // MARK: - @IBOutlets
    
    @IBOutlet private weak var productName : UILabel!
    @IBOutlet private weak var productCount: UILabel!
    @IBOutlet private weak var productImage: UIImageView!
    
     // MARK: - Properties
    
    public var buttonTappedAction : ((Int) -> Void)?
    
    // MARK: - Override func
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    // MARK: - Public func
    
    public func fill(withProduct product: ProductModel = ProductModel()) {
        self.productName?.text = product.title
        self.productCount.text = "\(String(describing: product.count!))"
        ImageHelper().loadImage(whithProduct: product, imageView: productImage)
    }
    
    // MARK: - @IBAction
    
    @IBAction private func stepperAction(_ sender: UIStepper) {
        self.productCount.text = "\(Int(sender.value))"
        self.buttonTappedAction?(Int(sender.value))
    }
}

