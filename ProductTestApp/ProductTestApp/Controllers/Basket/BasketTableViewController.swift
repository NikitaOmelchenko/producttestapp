//
//  BasketTableViewController.swift
//  ProductTestApp
//
//  Created by Nikita Omelchenko on 6/11/19.
//  Copyright © 2019 Nikita Omelchenko. All rights reserved.
//

import UIKit
import PKHUD

class BasketTableViewController: UITableViewController {
    
    // MARK: - @IBOutlets
    
    @IBOutlet weak var payButton: UIBarButtonItem!
    
    // MARK: - Properties
    
    private let cellHeight: CGFloat = cellHeightConst
    private let cellIdentificator = "BasketTableViewCell"
    
    // MARK: - Life cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        payButton.isEnabled = !User.isGuest
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        guard !User.isGuest else { return }
        User.model.updateSelectedProductList()
    }

    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if User.model == nil || User.model.selectedProducts.count == 0 {
            self.tableView.setEmptyMessage()
        } else {
            self.tableView.restore()
        }
        
        return User.model != nil ? User.model.selectedProducts.count : 0
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.cellHeight
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: BasketTableViewCell = tableView.dequeueReusableCell(withIdentifier: self.cellIdentificator, for: indexPath) as? BasketTableViewCell ?? BasketTableViewCell()
        let product = User.model.selectedProducts[indexPath.row]
        
        cell.fill(withProduct: product)
        cell.buttonTappedAction = { (IntVlue) in
            User.model.selectedProducts[indexPath.row].count = IntVlue
        }
        
        return cell
    }
}

// MARK: - @IBActions

private extension BasketTableViewController {
    
    @IBAction func rentAction(_ sender: Any) {
        User.model.selectedProducts.removeAll()
        HUD.flash(.success, onView: self.view, delay: animationDurationStandart, completion: { (_) in
            self.navigationController?.popViewController(animated: true)
        })
    }
}
