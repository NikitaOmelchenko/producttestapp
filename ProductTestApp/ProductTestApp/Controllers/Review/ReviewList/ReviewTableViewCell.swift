//
//  ReviewTableViewCell.swift
//  ProductTestApp
//
//  Created by Nikita Omelchenko on 6/8/19.
//  Copyright © 2019 Nikita Omelchenko. All rights reserved.
//

import UIKit

class ReviewTableViewCell: UITableViewCell {
    
    // MARK: - @IBOutlets
    
    @IBOutlet private weak var reviewView : ReviewView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    public func fill(_ reviewModel: ReviewModel) {
        self.reviewView.fillReview(withReview: reviewModel)
    }

}
