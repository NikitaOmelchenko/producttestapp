//
//  ReviewListTableViewController.swift
//  ProductTestApp
//
//  Created by Nikita Omelchenko on 6/5/19.
//  Copyright © 2019 Nikita Omelchenko. All rights reserved.
//

import UIKit

class ReviewListTableViewController: UITableViewController {
    
    // MARK: - @IBOutlets
    
    @IBOutlet weak var creteReviewButton: UIBarButtonItem!
    // MARK: - Properties
    
    private let cellIdentificator = "reviewCommentCell"
    private let tableViewCellHeight: CGFloat = cellHeightConst
    
    private var ratingViewController: RatingViewController!
    private var reviewList: Array<ReviewModel> = [] {
        didSet {
            guard self.reviewList.count != 0 else { return }
            self.ratingViewController.fillRating(withReviews: self.reviewList)
            self.tableView.reloadSections(IndexSet(integersIn: 0...0), with: UITableView.RowAnimation.fade)
        }
    }
    
    public var productDetailDataSource: ProductDetailDataSource! {
        didSet {
            self.productDetailDataSource.productDetailDelegate = self
        }
    }
    
    // MARK: - Life cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.config()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.getDataSource()
    }

    // MARK: - Table view data source
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.reviewList.count == 0 {
            self.tableView.setEmptyMessage()
        } else {
            self.tableView.restore(.none)
        }
        return self.reviewList.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: ReviewTableViewCell = tableView.dequeueReusableCell(withIdentifier: self.cellIdentificator, for: indexPath) as? ReviewTableViewCell ?? ReviewTableViewCell()
        let revieModel = self.reviewList[indexPath.row]
        
        cell.fill(revieModel)
        
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? RatingViewController,
            segue.identifier == SegueIdentifier.toRatingViewControllerEmbedToListSegue.rawValue {
            self.ratingViewController = vc
        }
        
        if let vc = segue.destination as? CreateReviewViewController,
            segue.identifier == SegueIdentifier.toCreateReview.rawValue {
            vc.productDetailDataSource = self.productDetailDataSource
        }
    }
}

// MARK: - Product Review Data Source Protocol

extension ReviewListTableViewController: ProductReviewDataSourceProtocol {
    func fillWithDataSource(_ dataInfo: [ReviewModel], product: ProductModel) {
        self.reviewList = dataInfo
    }
}

// MARK: - Private funcs

private extension ReviewListTableViewController {
    private func config() {
        self.tableView.estimatedRowHeight = self.tableViewCellHeight
        self.creteReviewButton.isEnabled = !User.isGuest
        self.ratingViewController.fillRating(withReviews: self.reviewList)
    }
    
    private func getDataSource() {
        self.productDetailDataSource.getReviewList()
    }
}
