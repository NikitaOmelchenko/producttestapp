//
//  CreateReviewViewController.swift
//  ProductTestApp
//
//  Created by Nikita Omelchenko on 6/5/19.
//  Copyright © 2019 Nikita Omelchenko. All rights reserved.
//

import UIKit
import Cosmos

class CreateReviewViewController: UIViewController {
    
    // MARK: - @IBOutlets
    
    @IBOutlet private weak var ratingView       : CosmosView!
    @IBOutlet private weak var commentTextView  : PlaceHolderTextView!
    @IBOutlet private weak var bottomTextView   : NSLayoutConstraint!
    
    // MARK: - Properties
    
    private let keyboardHelper   = KeyboardHelper()
    
    public var productDetailDataSource: ProductDetailDataSource! {
        didSet {
            self.productDetailDataSource.createReviewDelegate = self
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.config()
    }
}

// MARK: - Product Review Data Source Protocol

extension CreateReviewViewController: CreateReviewDataSourceProtocol {
    func createReview() {
        self.navigationController?.popViewController(animated: true)
    }
}

// MARK: - @IBActions

private extension CreateReviewViewController {
    
    @IBAction func cancelAction(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func createReviewAction(_ sender: UIBarButtonItem) {
        self.productDetailDataSource.createReview(ratingView.rating, comment: self.commentTextView.text ?? "")
    }
}

// MARK: - Private func

private extension CreateReviewViewController {
    
    private func config() {
        self.commentTextView.addDoneButton(target: self, selector: #selector(tapDone(sender:)))
        self.addKeyboardNotifications()
    }
    
    @objc func tapDone(sender: Any) {
        self.view.endEditing(true)
    }
    
    // MARK: - Keyboard Notifications
    
    private func addKeyboardNotifications() {
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShowBottom), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHideBottom), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc private func keyboardWillShowBottom(notification: NSNotification) {
        self.keyboardHelper.keyboardWillShowBottom(notification: notification, constraint: &self.bottomTextView.constant)
        UIView.animate(withDuration: animationDurationStandart) { self.view.layoutIfNeeded() }
    }
    
    @objc private func keyboardWillHideBottom(notification: NSNotification) {
        self.keyboardHelper.keyboardWillHideBottom(notification: notification, constraint: &self.bottomTextView.constant)
        UIView.animate(withDuration: animationDurationStandart) { self.view.layoutIfNeeded() }
    }
}

