//
//  SignInTextFieldDelegate.swift
//  ProductTestApp
//
//  Created by Nikita Omelchenko on 6/5/19.
//  Copyright © 2019 Nikita Omelchenko. All rights reserved.
//

import UIKit

extension SignInViewController: UITextFieldDelegate {
    
    // MARK: - UITextField Delegate
    
    @IBAction func done(_ sender: UITextField) {
        sender.resignFirstResponder()
    }
}
