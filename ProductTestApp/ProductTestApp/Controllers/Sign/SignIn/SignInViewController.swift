//
//  SignInViewController.swift
//  ProductTestApp
//
//  Created by Nikita Omelchenko on 6/5/19.
//  Copyright © 2019 Nikita Omelchenko. All rights reserved.
//

import UIKit

class SignInViewController: UIViewController {
    
    // MARK: - @IBOutlets
    
    @IBOutlet weak var loginTextField   : UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var yPositioStackView: NSLayoutConstraint!
    
    // MARK: - Properties
    
    public  var signInDataSource = SignInDataSource()
    private let keyboardHelper   = KeyboardHelper()

    
    // MARK: - Life cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        self.configScreen()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.clearInputs()
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard segue.identifier == SegueIdentifier.toSignUpScreen.rawValue else { return }
        let vc: SignUpViewController = segue.destination as! SignUpViewController
        vc.delegate = self
    }
}

// MARK: - Sign Up Protocol

extension SignInViewController: SignUpProtocol {
    func signInAfterSignUp(withUser user: UserModel!) {
        self.signInDataSource.signIn(withLogin: user.username, password: user.password)
    }
}

// MARK: - Sign In Data Source Protocol

extension SignInViewController: SignInDataSourceProtocol {
    func signInSuccess() {
        self.performSegue(withIdentifier: SegueIdentifier.toProductListScreen.rawValue, sender: self)
    }
}

// MARK: - @IBActions

private extension SignInViewController {
    
    @IBAction func signInAction(_ sender: UIButton) {
        self.signInDataSource.signIn(withLogin: self.loginTextField.text ?? "", password: self.passwordTextField.text ?? "")
    }
    
    @IBAction func signInGuestAction(_ sender: UIButton) {
        self.performSegue(withIdentifier: SegueIdentifier.toProductListScreen.rawValue, sender: nil)
    }
}

// MARK: - Private func

private extension SignInViewController {
    
    private func configScreen() {
        self.addKeyboardNotifications()
        self.signInDataSource.delegate = self
    }
    
    private func clearInputs() {
        self.loginTextField.text?.removeAll()
        self.passwordTextField.text?.removeAll()
    }
    
    // MARK: - Keyboard Notifications
    
    private func addKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc private func keyboardWillShow(notification: NSNotification) {
        self.keyboardHelper.keyboardWillShow(notification: notification, constraint: &self.yPositioStackView.constant)
        UIView.animate(withDuration: animationDurationStandart) { self.view.layoutIfNeeded() }
    }
    
    @objc private func keyboardWillHide(notification: NSNotification) {
        self.keyboardHelper.keyboardWillHide(notification: notification, constraint: &self.yPositioStackView.constant)
        UIView.animate(withDuration: animationDurationStandart) { self.view.layoutIfNeeded() }
    }
}
