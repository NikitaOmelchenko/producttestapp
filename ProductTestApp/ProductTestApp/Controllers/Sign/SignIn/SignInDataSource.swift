//
//  SignInDataSource.swift
//  ProductTestApp
//
//  Created by Nikita Omelchenko on 6/5/19.
//  Copyright © 2019 Nikita Omelchenko. All rights reserved.
//

import UIKit

// MARK: - Protocol

protocol SignInDataSourceProtocol: AnyObject {
    func signInSuccess()
}

// MARK: - Class

struct SignInDataSource {
    
    // MARK: - Properties
    
    private let validateHelper = ValidateHelper()
    
    // MARK: - SignInDataSourceDelegate
    
    public weak var delegate: SignInDataSourceProtocol!
    
    // MARK: - Private func
    
    private func validateSignIn(withLogin login: String, password: String) -> Bool {
        guard validateHelper.isLoginValid(login) == true else { return false }
        guard validateHelper.isPasswordValid(password) == true else { return false }
        return true
    }
    
    // MARK: - Public func
    
    public func signIn(withLogin login: String, password: String) {
        let userSignIn = UserModel(withLogin: login, password: password)
        
        guard self.validateSignIn(withLogin: login, password: password) == true else { return }
        SignRequests().signIn(withUser: userSignIn, success: { 
            User.model = userSignIn
            self.delegate.signInSuccess()
        })
    }
}
