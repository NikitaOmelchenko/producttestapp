//
//  SignUpTextFieldDelegate.swift
//  ProductTestApp
//
//  Created by Nikita Omelchenko on 6/6/19.
//  Copyright © 2019 Nikita Omelchenko. All rights reserved.
//

import UIKit

// MARK: - UITextField Delegate

extension SignUpViewController: UITextFieldDelegate {
    
    @IBAction func done(_ sender: UITextField) {
        sender.resignFirstResponder()
    }
}
