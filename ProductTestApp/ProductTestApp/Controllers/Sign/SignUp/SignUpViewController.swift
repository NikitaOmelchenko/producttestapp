//
//  SignUpViewController.swift
//  ProductTestApp
//
//  Created by Nikita Omelchenko on 6/5/19.
//  Copyright © 2019 Nikita Omelchenko. All rights reserved.
//

import UIKit

// MARK: - Protocol

protocol SignUpProtocol: AnyObject {
    func signInAfterSignUp(withUser user: UserModel!)
}

// MARK: - Class

class SignUpViewController: UIViewController {

    // MARK: - @IBOutlets
    
    @IBOutlet private weak var loginTextField           : UITextField!
    @IBOutlet private weak var passwordTextField        : UITextField!
    @IBOutlet private weak var passwordConfirmTextField : UITextField!
    @IBOutlet private weak var yPositioStackView        : NSLayoutConstraint!
    
    // MARK: - Properties
    
    private let signUpDataSource = SignUpDataSource()
    private let keyboardHelper   = KeyboardHelper()
    
    // MARK: - SignUpDelegate
    
    public weak var delegate: SignUpProtocol?
    
    // MARK: - Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.configScreen()
    }
}

// MARK: - Sign Up Data Source Protocol

extension SignUpViewController: SignUpDataSourceProtocol {
    func signUp() {
        self.dismiss(animated: true, completion: {
            self.delegate?.signInAfterSignUp(withUser: User.model)
        })
    }
}

// MARK: - @IBActions

private extension SignUpViewController {
    
    @IBAction func signUpAction(_ sender: UIButton) {
        self.signUpDataSource.signUp(withLogin: self.loginTextField.text ?? "", password: self.passwordTextField.text ?? "", confirmPassword: self.passwordConfirmTextField.text ?? "")
    }
    
    @IBAction func cancelAction(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
}

 // MARK: - Private func

private extension SignUpViewController {
    
    private func configScreen() {
        self.addKeyboardNotifications()
        self.signUpDataSource.delegate = self
    }
    
    // MARK: - Keyboard Notifications
    
    private func addKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc private func keyboardWillShow(notification: NSNotification) {
        self.keyboardHelper.keyboardWillShow(notification: notification, constraint: &self.yPositioStackView.constant)
        UIView.animate(withDuration: animationDurationStandart) { self.view.layoutIfNeeded() }
    }
    
    @objc private func keyboardWillHide(notification: NSNotification) {
        self.keyboardHelper.keyboardWillHide(notification: notification, constraint: &self.yPositioStackView.constant)
        UIView.animate(withDuration: animationDurationStandart) { self.view.layoutIfNeeded() }
    }
}
