//
//  SignUpDataSource.swift
//  ProductTestApp
//
//  Created by Nikita Omelchenko on 6/6/19.
//  Copyright © 2019 Nikita Omelchenko. All rights reserved.
//

import UIKit

// MARK: - Protocol

protocol SignUpDataSourceProtocol: AnyObject {
    func signUp()
}

// MARK: - Class

class SignUpDataSource {
    
    // MARK: - Properties
    
    private let validateHelper = ValidateHelper()
    
    // MARK: - SignUpDataSourceDelegate
    
    public weak var delegate: SignUpDataSourceProtocol!
    
    // MARK: - Private func
    
    private func validateSignUp(withLogin login: String, password: String, confirmPassword: String) -> Bool {
        guard validateHelper.isLoginValid(login) == true else { return false }
        guard validateHelper.isPasswordSignUpValid(password, confirmPassword: confirmPassword) == true else { return false }
        return true
    }
    
    // MARK: - Public func
    
    public func signUp(withLogin login: String, password: String, confirmPassword: String) {
        let userSignUp = UserModel(withLogin: login, password: password)
        
        guard self.validateSignUp(withLogin: login, password: password, confirmPassword: confirmPassword) == true else { return }
        SignRequests().signUp(withUser: userSignUp, success: {
            User.model = userSignUp
            self.delegate.signUp()
        })
    }
}
