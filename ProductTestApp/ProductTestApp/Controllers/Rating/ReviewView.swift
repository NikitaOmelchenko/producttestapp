//
//  ReviewView.swift
//  ProductTestApp
//
//  Created by Nikita Omelchenko on 6/8/19.
//  Copyright © 2019 Nikita Omelchenko. All rights reserved.
//

import UIKit
import Cosmos

class ReviewView: UIView {
    
    // MARK: - @IBOutlets
    
    @IBOutlet private weak var userNameLabel    : UILabel!
    @IBOutlet private weak var commentLabel     : UILabel!
    @IBOutlet private weak var ratingStarsView  : CosmosView!
    
    public func fillReview(withReview reviewModel: ReviewModel) {
        self.userNameLabel.text = reviewModel.created_by.username
        self.commentLabel.text  = reviewModel.text
        ratingStarsView.rating  = Double(reviewModel.rate ?? 0)
    }
}
