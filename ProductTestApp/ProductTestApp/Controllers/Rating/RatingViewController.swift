//
//  RatingViewController.swift
//  ProductTestApp
//
//  Created by Nikita Omelchenko on 6/7/19.
//  Copyright © 2019 Nikita Omelchenko. All rights reserved.
//

import UIKit

class RatingViewController: UIViewController {
    
    // MARK: - @IBOutlets
    
    @IBOutlet private weak var ratingLabel      : UILabel!
    @IBOutlet private weak var countReview      : UILabel!
    @IBOutlet private weak var oneStarProgress  : UIProgressView!
    @IBOutlet private weak var twoStarProgress  : UIProgressView!
    @IBOutlet private weak var threeStarProgress: UIProgressView!
    @IBOutlet private weak var fourStarProgress : UIProgressView!
    @IBOutlet private weak var fiveStarProgress : UIProgressView!
    
    enum CountStars: Int {
        case one    = 1
        case two    = 2
        case three  = 3
        case four   = 4
        case five   = 5
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // MARK: - Public func

    public func fillRating(withReviews reviews: [ReviewModel]) {
        self.setRtingLabelText(reviews)
        self.setProgress(reviews)
    }
    
    // MARK: - Privte func
    
    private func setRtingLabelText(_ reviews: [ReviewModel]) {
        self.countReview.text = "\(reviews.count) ratirng"
        self.setRateLabelText(reviews)
        
    }
    
    private func setRateLabelText(_ reviews: [ReviewModel]) {
        var summRating = 0
        _ = reviews.map {summRating += $0.rate}
        let rating: Float = Float(summRating) / Float(reviews.count)
        self.ratingLabel.text = String(format: "%.1f", rating)
    }
    
    private func setProgress(_ reviews: [ReviewModel]) {
        self.oneStarProgress.setProgress    (calculateProgress(reviews, countStars: .one),   animated: true)
        self.twoStarProgress.setProgress    (calculateProgress(reviews, countStars: .two),   animated: true)
        self.threeStarProgress.setProgress  (calculateProgress(reviews, countStars: .three), animated: true)
        self.fourStarProgress.setProgress   (calculateProgress(reviews, countStars: .four),  animated: true)
        self.fiveStarProgress.setProgress   (calculateProgress(reviews, countStars: .five),  animated: true)
    }
    
    private func calculateProgress(_ reviews: [ReviewModel], countStars: CountStars) -> Float {
        let ratingCount = reviews.filter() {$0.rate == countStars.rawValue}.count
        let ratingProgressCount = Float(ratingCount) / Float(reviews.count)
        return ratingProgressCount
    }
    
    
}
