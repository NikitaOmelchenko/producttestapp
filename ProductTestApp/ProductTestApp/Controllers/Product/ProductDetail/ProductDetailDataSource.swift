//
//  ProductDetailDataSource.swift
//  ProductTestApp
//
//  Created by Nikita Omelchenko on 6/7/19.
//  Copyright © 2019 Nikita Omelchenko. All rights reserved.
//

import UIKit
import PKHUD

// MARK: - Protocol

protocol ProductReviewDataSourceProtocol: AnyObject {
    func fillWithDataSource(_ dataInfo: [ReviewModel], product: ProductModel)
}

protocol CreateReviewDataSourceProtocol: AnyObject {
    func createReview()
}

// MARK: - Class

class ProductDetailDataSource {
    // MARK: - Properties
    public var product: ProductModel = ProductModel()
    
    private var reviewList: [ReviewModel] = [] {
        didSet {
            self.productDetailDelegate.fillWithDataSource(self.reviewList, product: self.product)
        }
    }
    
    private var review: ReviewModel!
    
    // MARK: - ProductReviewDataSourceDelegte
    
    public weak var productDetailDelegate: ProductReviewDataSourceProtocol!
    public weak var createReviewDelegate: CreateReviewDataSourceProtocol!
    
    // MARK: - Private func
    
    private func createNewReview(_ rate: Double, comment: String) -> ReviewModel {
        let newReview = ReviewModel(product: self.product.id,
                                    rate: Int(rate),
                                    text: comment,
                                    createdBy: Created(username: User.model.username))
        return newReview
    }
    
    // MARK: - Public func
    
    public func getReviewList() {
        ReviewRequests().getReviewList(withProduct: self.product, success: { (reviewInfoList) in
            self.reviewList = reviewInfoList.sorted(by: { $0.id > $1.id })
        })
    }
    
    public func createReview(_ rate: Double, comment: String) {
        ReviewRequests().createReview(withReview: self.createNewReview(rate, comment: comment), success: {
            HUD.flash(.success, onView: UIApplication.topViewController()?.view, delay: animationDurationStandart, completion: { (_) in
                self.createReviewDelegate.createReview()
            })
        }) {
            HUD.flash(.error)
        }
    }
    
    
}
