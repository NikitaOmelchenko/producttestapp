//
//  ProductDetailTableViewController.swift
//  ProductTestApp
//
//  Created by Nikita Omelchenko on 6/7/19.
//  Copyright © 2019 Nikita Omelchenko. All rights reserved.
//

import UIKit

class ProductDetailTableViewController: UITableViewController {
    
    // MARK: - @IBOutlets
    
    @IBOutlet private weak var showAllButton    : UIButton!
    @IBOutlet private weak var creteReviewButton: UIButton!
    @IBOutlet private weak var productImageView : UIImageView!
    @IBOutlet private weak var nameProduct      : UILabel!
    @IBOutlet private weak var infoProduct      : UILabel!
    @IBOutlet private weak var addProductButton : UIBarButtonItem!
    @IBOutlet private weak var reviewView       : ReviewView!
    
    // MARK: - Properties
    
    private enum CountProductDetailTableView: Int {
        case countOfSections        = 4
        case lastSection            = 3
        case countCellsInSection    = 1
        case counCellsInLastSection = 2
    }
    
    public var productDetailDataSource: ProductDetailDataSource = ProductDetailDataSource() {
        didSet {
            self.productDetailDataSource.productDetailDelegate = self
        }
    }
    
    private var ratingViewController: RatingViewController!
    private let tableViewCellHeight: CGFloat = cellHeightConst
    
    private var product: ProductModel! {
        didSet {
            self.nameProduct.text = self.product.title
            self.infoProduct.text = self.product.text
            ImageHelper().loadImage(whithProduct: self.product, imageView: productImageView)
            
            if User.isGuest {
                self.addProductButton.isEnabled = !User.isGuest
            } else if !User.model.isAddInList(self.product) {
                self.addProductButton.isEnabled = true
            } else {
                self.addProductButton.isEnabled = false
            }
        }
    }
    private var reviewList: Array<ReviewModel> = [] {
        didSet {
            guard self.reviewList.count != 0 else { return }
            self.ratingViewController.fillRating(withReviews: self.reviewList)
            self.reviewView.fillReview(withReview: self.reviewList.first!)
            self.tableView.reloadSections(IndexSet(integersIn: 2...CountProductDetailTableView.lastSection.rawValue), with: UITableView.RowAnimation.none)
        }
    }
    
    // MARK: - Life cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.config()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.productDetailDataSource.productDetailDelegate = self 
        self.getDataSource()
    }

    // MARK: - Table view data source
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return CountProductDetailTableView.countOfSections.rawValue
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == CountProductDetailTableView.lastSection.rawValue {
            return CountProductDetailTableView.counCellsInLastSection.rawValue
        }
        return CountProductDetailTableView.countCellsInSection.rawValue
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? RatingViewController,
            segue.identifier == SegueIdentifier.toRatingViewControllerEmbedSegue.rawValue {
            self.ratingViewController = vc
        }
        
        if let vc = segue.destination as? ReviewListTableViewController,
            segue.identifier == SegueIdentifier.toReviewList.rawValue {
            vc.productDetailDataSource = self.productDetailDataSource
        }
        
        if let vc = segue.destination as? CreateReviewViewController,
            segue.identifier == SegueIdentifier.toCreateReviewFromDetail.rawValue {
            vc.productDetailDataSource = self.productDetailDataSource
        }
    }
}

// MARK: - Product Review Data Source Protocol

extension ProductDetailTableViewController: ProductReviewDataSourceProtocol {
    func fillWithDataSource(_ dataInfo: [ReviewModel], product: ProductModel) {
        self.product = product
        self.reviewList = dataInfo
    }
}

// MARK: - @IBActions

private extension ProductDetailTableViewController {
    
    @IBAction func selectedToBgAction(_ sender: UIBarButtonItem) {
        self.addProductButton.isEnabled = false
        guard !User.isGuest else { return }
        User.model.addProductSelcted(product)
    }
    
    @objc func showAllAction(_ sender: UIButton) {
        self.performSegue(withIdentifier: SegueIdentifier.toReviewList.rawValue, sender: self)
    }
    
    @objc func createReviewAction(_ sender: UIButton) {
        self.performSegue(withIdentifier: SegueIdentifier.toCreateReviewFromDetail.rawValue, sender: self)
    }
}

// MARK: - Private funcs

private extension ProductDetailTableViewController {
    
    private func config() {
        self.tableView.estimatedRowHeight = self.tableViewCellHeight
        self.showAllButton.addTarget(self, action: #selector(showAllAction(_:)), for: .touchUpInside)
        self.creteReviewButton.addTarget(self, action: #selector(createReviewAction(_:)), for: .touchUpInside)
        self.creteReviewButton.isHidden = User.isGuest
    }
    
    private func getDataSource() {
        self.productDetailDataSource.getReviewList()
    }
}
