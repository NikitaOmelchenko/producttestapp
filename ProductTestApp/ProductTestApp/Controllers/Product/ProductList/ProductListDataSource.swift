//
//  ProductListDataSource.swift
//  ProductTestApp
//
//  Created by Nikita Omelchenko on 6/7/19.
//  Copyright © 2019 Nikita Omelchenko. All rights reserved.
//

import UIKit

// MARK: - Protocol

protocol ProductListDataSourceProtocol: AnyObject {
    func fillWithDataSource(withInfo dataInfo: [ProductModel])
}

// MARK: - Class

class ProductListDataSource {
    
    // MARK: - Properties
    
    private var dataInfo: [ProductModel] = [] {
        didSet {
            self.delegate.fillWithDataSource(withInfo: self.dataInfo)
        }
    }
    
    // MARK: - ProductListDataSourceDelegate
    
    public weak var delegate: ProductListDataSourceProtocol!
    
    // MARK: - Private func
    
    // MARK: - Public func
    public func getProductList() {
        ProductRequests().getProductList({ data in
            self.dataInfo = data
        })
    }
    
    public func sortDataSource(withSearchText text: String) -> [ProductModel] {
        return text.isEmpty ? self.dataInfo : self.dataInfo.filter { $0.title.contains(text) }
    }
    
}

