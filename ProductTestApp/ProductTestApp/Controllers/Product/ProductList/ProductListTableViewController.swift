//
//  ProductListTableViewController.swift
//  ProductTestApp
//
//  Created by Nikita Omelchenko on 6/5/19.
//  Copyright © 2019 Nikita Omelchenko. All rights reserved.
//

import UIKit

class ProductListTableViewController: UITableViewController {
    
    // MARK: - @IBOutlets
    
    // MARK: - Properties
    
    private let cellHeight: CGFloat = cellHeightConst
    private let cellIdentificator = "ProductListTableViewCell"

    private var selectedProduct: ProductModel = ProductModel()
    
    public var productListDataSource = ProductListDataSource()
    public var dataSource: [ProductModel] = [] {
        didSet {
            self.tableView.reloadSections(IndexSet(integersIn: 0...0), with: UITableView.RowAnimation.automatic)
        }
    }
    
    // MARK: - Life cycle

    override func viewDidLoad() {
        super.viewDidLoad()

        self.config()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.getDataSource()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard segue.identifier == SegueIdentifier.toProductInfoScreen.rawValue else { return }
        let vc: ProductDetailTableViewController = segue.destination as! ProductDetailTableViewController
        vc.productDetailDataSource = ProductDetailDataSource()
        vc.productDetailDataSource.product = selectedProduct
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if self.dataSource.count == 0 {
            self.tableView.setEmptyMessage()
        } else {
            self.tableView.restore()
        }
        
        return self.dataSource.count
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.cellHeight
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: ProductListTableViewCell = tableView.dequeueReusableCell(withIdentifier: self.cellIdentificator, for: indexPath) as? ProductListTableViewCell ?? ProductListTableViewCell()
        let product = self.dataSource[indexPath.row]
        
        cell.fill(withProduct: product)
    
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedProduct = self.dataSource[indexPath.row]
        self.performSegue(withIdentifier: SegueIdentifier.toProductInfoScreen.rawValue, sender: self)
    }
    
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    override func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        return self.creteSwipeAction(withTitle: "Show info", color: .black, style: .normal, complition: {
            self.tableView(self.tableView, didSelectRowAt: indexPath)
        })
    }
    
    override func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        return self.creteSwipeAction(complition: { })
    }
    
}

// MARK: - Product List Data Source Protocol

extension ProductListTableViewController: ProductListDataSourceProtocol {
    
    func fillWithDataSource(withInfo dataInfo: [ProductModel]) {
        self.dataSource = dataInfo
    }
}

// MARK: - @IBActions

private extension ProductListTableViewController {

    @IBAction func logoutAction(_ sender: UIBarButtonItem) {
        UserDefaultsHelper.setUserProducts()
        User.model = nil
        self.navigationController?.popToRootViewController(animated: true)
    }
}

// MARK: - Private func

private extension ProductListTableViewController {
    
    private func getDataSource() {
        self.productListDataSource.getProductList()
    }
    
    private func config() {
        self.productListDataSource.delegate = self
        
        self.configTableView()
        self.configSearchBar()
    }
    
    private func configSearchBar() {
        let search = UISearchController(searchResultsController: nil)
        search.searchResultsUpdater = self
        search.delegate = self
        search.obscuresBackgroundDuringPresentation = false
        search.searchBar.tintColor = .black
        self.navigationItem.searchController = search
    }
    
    private func configTableView() {
        self.clearsSelectionOnViewWillAppear = true
        self.tableView.refreshControl!.addTarget(self, action: #selector(refreshProductData(_:)), for: .valueChanged)
    }
    
    private func creteSwipeAction(withTitle title: String = "Delete", color: UIColor = .red, style: UIContextualAction.Style = .destructive, complition: @escaping () -> Void) -> UISwipeActionsConfiguration {
        let action =  UIContextualAction(style: style, title: title, handler: { (action, view, completionHandler ) in
            complition()
            completionHandler(true)
        })
        action.backgroundColor = color
        let confrigation = UISwipeActionsConfiguration(actions: [action])
        
        return confrigation
    }
    
    @objc private func refreshProductData(_ sender: Any) {
        self.getDataSource()
        self.tableView.refreshControl?.endRefreshing()
    }
}
