//
//  ProductListUISearchControllerDelegate.swift
//  ProductTestApp
//
//  Created by Nikita Omelchenko on 6/7/19.
//  Copyright © 2019 Nikita Omelchenko. All rights reserved.
//

import UIKit

extension ProductListTableViewController: UISearchResultsUpdating {
    
    func updateSearchResults(for searchController: UISearchController) {
        guard let text = searchController.searchBar.text else { return }
        guard text.count > 0 else { return }
        self.dataSource = self.productListDataSource.sortDataSource(withSearchText: text)
    }
}

// MARK: - 

extension ProductListTableViewController: UISearchControllerDelegate {
    func willDismissSearchController(_ searchController: UISearchController) {
        self.dataSource = self.productListDataSource.sortDataSource(withSearchText: "")
    }
}
