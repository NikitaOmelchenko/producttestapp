//
//  ProductListTableViewCell.swift
//  ProductTestApp
//
//  Created by Nikita Omelchenko on 6/7/19.
//  Copyright © 2019 Nikita Omelchenko. All rights reserved.
//

import UIKit
import Kingfisher

class ProductListTableViewCell: UITableViewCell {
    
    // MARK: - @IBOutlets
    
    @IBOutlet private weak var productName : UILabel!
    @IBOutlet private weak var productImage: UIImageView!
    
    // MARK: - Override func

    override func awakeFromNib() {
        super.awakeFromNib()

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    // MARK: - Private func
    
    private func config() {
        self.accessoryType = .disclosureIndicator
    }
    
    // MARK: - Public func
    
    public func fill(withProduct product: ProductModel = ProductModel()) {
        self.config()
        
        self.productName?.text = product.title
        ImageHelper().loadImage(whithProduct: product, imageView: productImage)
    }
}
