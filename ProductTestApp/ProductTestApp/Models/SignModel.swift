//
//  SignModel.swift
//  ProductTestApp
//
//  Created by Nikita Omelchenko on 6/6/19.
//  Copyright © 2019 Nikita Omelchenko. All rights reserved.
//

import UIKit

struct SignModel: Codable {
    var success: Bool!
    var token  : String!
    var message: String!
}
