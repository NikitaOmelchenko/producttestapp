//
//  ReviewModel.swift
//  ProductTestApp
//
//  Created by Nikita Omelchenko on 6/7/19.
//  Copyright © 2019 Nikita Omelchenko. All rights reserved.
//

import UIKit

struct ReviewModel: Codable {
    var id          : Int!
    var product     : Int!
    var rate        : Int!
    var text        : String!
    var created_by  : Created!
    
    init(with id: Int? = nil, product: Int?, rate: Int?, text: String?, createdBy: Created?) {
        self.id         = id
        self.product    = product
        self.rate       = rate
        self.text       = text
        self.created_by = createdBy
    }
}

struct Created: Codable {
    var id      : Int!
    var username: String!
    
    init(with id: Int? = nil, username: String?) {
        self.id         = id
        self.username   = username
    }
}

struct CreteReviewParams: Codable {
    var rate: Int!
    var text: String!
}
