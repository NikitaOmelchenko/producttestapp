//
//  ProductModel.swift
//  ProductTestApp
//
//  Created by Nikita Omelchenko on 6/6/19.
//  Copyright © 2019 Nikita Omelchenko. All rights reserved.
//

import UIKit

struct ProductModel: Codable, Hashable {
    var id      : Int!
    var count   : Int! = 0
    var img     : String!
    var text    : String!
    var title   : String!
    
    static func == (lhs: ProductModel, rhs: ProductModel) -> Bool {
        return lhs.id == rhs.id && lhs.img == rhs.img && lhs.text == rhs.text && lhs.title == rhs.title
    }
}
