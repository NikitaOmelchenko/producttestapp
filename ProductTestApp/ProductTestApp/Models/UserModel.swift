//
//  UserModel.swift
//  ProductTestApp
//
//  Created by Nikita Omelchenko on 6/5/19.
//  Copyright © 2019 Nikita Omelchenko. All rights reserved.
//

import UIKit

struct User {
    static var model: UserModel! 
    static var isGuest: Bool {
        get {
            return model == nil  ? true : false
        }
    }
}

struct UserModel: Codable {
    var username     : String!
    var password     : String!
    var selectedProducts: [ProductModel] = []
    
    init(withLogin login: String, password: String) {
        self.username   = login
        self.password   = password
        UserDefaultsHelper.getUserProducts(&self)
    }
    
    public mutating func addProductSelcted(_ product: ProductModel) {
        var userProduct = product
        userProduct.count = 1
        self.selectedProducts.append(userProduct)
        self.updateSelectedProductList()
    }
    
    public mutating func updateSelectedProductList() {
        let filterProducts = self.selectedProducts.filter() { $0.count ?? 0 > 0 }
        let nsSetProducts = Set(filterProducts)
        self.selectedProducts = Array(nsSetProducts).sorted(by: { $0.id > $1.id })
    }
    
    public func isAddInList(_ product: ProductModel) -> Bool {
        let product = self.selectedProducts.filter() {$0.id == product.id}
        if product.count > 0 {
            return true
        }
        return false
    }
}


