//
//  String+Extension.swift
//  ProductTestApp
//
//  Created by Nikita Omelchenko on 6/6/19.
//  Copyright © 2019 Nikita Omelchenko. All rights reserved.
//

import UIKit
import SwiftHash

public extension String {
    func toMD5Hash() -> String {
        return MD5(self)
    }
}
