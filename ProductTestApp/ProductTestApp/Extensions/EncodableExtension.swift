//
//  Encodable+Extension.swift
//  ProductTestApp
//
//  Created by Nikita Omelchenko on 6/6/19.
//  Copyright © 2019 Nikita Omelchenko. All rights reserved.
//

import Foundation

public extension Encodable {
    func asDictionary() throws -> [String: Any] {
        let data = try JSONEncoder().encode(self)
        guard var dictionary = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: Any] else {
            throw NSError()
        }
        if let password: String = dictionary["password"] as? String {
            dictionary.updateValue(password.toMD5Hash(), forKey: "password")
        }
        return dictionary
    }
}

public extension Data {
    func toString() -> String {
        return String(decoding: self, as: UTF8.self)
    }
}
