//
//  UITextViewExtension.swift
//  ProductTestApp
//
//  Created by Nikita Omelchenko on 6/8/19.
//  Copyright © 2019 Nikita Omelchenko. All rights reserved.
//

import UIKit

extension UITextView {
    
    func addDoneButton(title: String = "Done", target: Any, selector: Selector) {
        
        let toolBar = UIToolbar(frame: CGRect(x: 0.0,
                                              y: 0.0,
                                              width: UIScreen.main.bounds.size.width,
                                              height: toolBarHeight))//1
        let flexible = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)//2
        let barButton = UIBarButtonItem(title: title, style: .plain, target: target, action: selector)//3
        toolBar.setItems([flexible, barButton], animated: false)//4
        barButton.tintColor = .black
        self.inputAccessoryView = toolBar//5
    }
}
