//
//  UIAlertController+Extension.swift
//  ProductTestApp
//
//  Created by Nikita Omelchenko on 6/5/19.
//  Copyright © 2019 Nikita Omelchenko. All rights reserved.
//

import UIKit
import Foundation

extension UIAlertController {
    func show() {
        if let topController = UIApplication.topViewController() {
            topController.present(self, animated: true, completion: nil)
        }
    }
    
    class func alert(title: String, msg: String) {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
            (result: UIAlertAction) -> Void in
        })
        alert.show()
    }
}
