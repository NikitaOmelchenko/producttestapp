//
//  ImageHelper.swift
//  ProductTestApp
//
//  Created by Nikita Omelchenko on 6/7/19.
//  Copyright © 2019 Nikita Omelchenko. All rights reserved.
//

import Foundation
import Kingfisher

struct ImageHelper {
    public func loadImage(whithProduct product: ProductModel, imageView: UIImageView!) {
        guard imageView != nil else { return }
        let placeholderImageName = "box"
        let imageName = product.img ?? ""
        let url = BaseUrl.imageHost.rawValue + imageName
        
        imageView.kf.indicatorType = .activity
        imageView.kf.setImage(with: URL(string: url), placeholder: UIImage(named: placeholderImageName))
    }
}
