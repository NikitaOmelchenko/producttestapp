//
//  AlertHelper.swift
//  ProductTestApp
//
//  Created by Nikita Omelchenko on 6/6/19.
//  Copyright © 2019 Nikita Omelchenko. All rights reserved.
//

import UIKit

struct AlertHelper {
    
    // MARK: - Login Error Alerts

    public func loginValidateErrorAlert() {
        UIAlertController.alert(title: "Error", msg: "Login must have only letters, underscores and numbers\nLength should be 18 characters max and 6 characters minimum")
    }
    
    public func loginValidateInSystemErrorAlert() {
        UIAlertController.alert(title: "Error", msg: "User with this login not found in the system")
    }
    
    public func loginValidateInSystemSigUpErrorAlert() {
        UIAlertController.alert(title: "Error", msg: "User with this login already in the system")
    }
    
    // MARK: - Paassword Error Alerts
    
    public func passwordValidateErrorAlert() {
        UIAlertController.alert(title: "Error", msg: "Pasword must have only letters, underscores and numbers\nLength should be 18 characters max and 6 characters minimum")
    }
    
    public func passwordValidateInSystemErrorAlert() {
        UIAlertController.alert(title: "Error", msg: "Wrong login or password")
    }
    
    public func passwordEqualValidateErrorAlert() {
        UIAlertController.alert(title: "Error", msg: "Pasword is not equal with confirm password")
    }
}
