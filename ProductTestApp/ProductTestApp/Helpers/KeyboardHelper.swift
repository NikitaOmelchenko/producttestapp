//
//  KeyboardHelper.swift
//  ProductTestApp
//
//  Created by Nikita Omelchenko on 6/6/19.
//  Copyright © 2019 Nikita Omelchenko. All rights reserved.
//

import UIKit

struct KeyboardHelper {
    
    public func keyboardWillShow(notification: NSNotification, constraint: inout CGFloat) {
        if let _ = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if constraint == 0 {
                constraint -= keyboardOffSetConstant
            }
        }
    }
    
    public func keyboardWillHide(notification: NSNotification, constraint: inout CGFloat) {
        if constraint != 0 {
            constraint = 0
        }
    }
    
    public func keyboardWillShowBottom(notification: NSNotification, constraint: inout CGFloat) {
        if let keyboardRect = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if constraint == bottomConstrain && keyboardRect.height <= 277 {
                constraint += keyboardRect.height + toolBarHeight
            } else {
                constraint += 277 + toolBarHeight 
            }
        }
    }
    
    public func keyboardWillHideBottom(notification: NSNotification, constraint: inout CGFloat) {
        if constraint != bottomConstrain {
            constraint = bottomConstrain
        }
    }
}
