//
//  UserDefaultsHelper.swift
//  ProductTestApp
//
//  Created by Nikita Omelchenko on 6/11/19.
//  Copyright © 2019 Nikita Omelchenko. All rights reserved.
//

import UIKit

struct UserDefaultsHelper {
    
    static let defaults = UserDefaults.standard
    
    static func setUserProducts() {
        guard !User.isGuest else { return }
        let key = User.model.username + User.model.password
        if User.model.selectedProducts.count > 0 {
            let encoder = JSONEncoder()
            if let encoded = try? encoder.encode(User.model) {
                defaults.set(encoded, forKey: key)
            }
        } else {
            defaults.removeObject(forKey: key)
        }
        
    }
    
    static func getUserProducts(_ userInfo: inout UserModel) {
//        guard !User.isGuest else { return }
        let key = userInfo.username + userInfo.password
        
        if let userWithProduct = defaults.object(forKey: key) as? Data {
            let decoder = JSONDecoder()
            if let loadedUserWithProduct = try? decoder.decode(UserModel.self, from: userWithProduct) {
                userInfo.selectedProducts = loadedUserWithProduct.selectedProducts
            }
        }
    }
}
