//
//  ValidateHelper.swift
//  ProductTestApp
//
//  Created by Nikita Omelchenko on 6/5/19.
//  Copyright © 2019 Nikita Omelchenko. All rights reserved.
//

import UIKit


struct ValidateHelper {
    
    // MARK: - Properties
    
    private let alertHelper             = AlertHelper()
    private let regularExpressionValid  = "\\A\\w{6,18}\\z"
    
    // MARK: - Login Validate
    
    public func isLoginValid(_ login: String) -> Bool {
        guard self.validateLogin(login) == true else { return false }
        return true
    }
    
    private func validateLogin(_ login: String) -> Bool {
        let isValid = login.range(of: regularExpressionValid, options: .regularExpression)
        
        guard isValid == nil else { return true }
        alertHelper.loginValidateErrorAlert()
        return false
    }
    
    // MARK: - Password Validate
    
    public func isPasswordValid(_ password: String) -> Bool {
        guard self.validatePassword(password) == true else { return false }
        return true
    }
    
    public func isPasswordSignUpValid(_ password: String, confirmPassword: String) -> Bool {
        guard self.validatePassword(password) == true else { return false }
        guard self.validateEqualPassword(password, confirmPassword: confirmPassword) == true else { return false }
        return true
    }
    
    private func validatePassword(_ password: String) -> Bool {
        let isValid = password.range(of: regularExpressionValid, options: .regularExpression)
        
        guard isValid == nil else { return true }
        alertHelper.passwordValidateErrorAlert()
        return false
    }
    
    private func validateEqualPassword(_ password: String, confirmPassword: String) -> Bool {
        let isValid = password.elementsEqual(confirmPassword)
        
        guard isValid == false else { return true }
        alertHelper.passwordEqualValidateErrorAlert()
        return false
    }
}
