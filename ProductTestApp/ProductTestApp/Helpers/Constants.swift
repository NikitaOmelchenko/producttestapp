//
//  Constants.swift
//  ProductTestApp
//
//  Created by Nikita Omelchenko on 6/5/19.
//  Copyright © 2019 Nikita Omelchenko. All rights reserved.
//

import Foundation
import UIKit

// MARK: - Constants

let productAppGuestName = "UserGuest4"

let keyboardOffSetConstant: CGFloat = 115
let toolBarHeight: CGFloat = 44
let bottomConstrain: CGFloat = 32
let cellHeightConst: CGFloat = 64
let animationDurationStandart: TimeInterval = 0.3

// MARK: - Enums

enum SegueIdentifier: String {
    case toProductInfoScreen = "showProductInfo"
    case toProductListScreen = "showProductList"
    case toSignUpScreen      = "showSignUpScreen"
    case toRatingViewControllerEmbedSegue = "RatingViewControllerEmbedSegue"
    case toRatingViewControllerEmbedToListSegue = "RatingViewControllerEmbedToListSegue"
    case toReviewList = "showReviewList"
    case toCreateReview = "CreateReview"
    case toCreateReviewFromDetail = "CreateReviewFromDetail"
}
