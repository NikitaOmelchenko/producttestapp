//
//  ApiManager.swift
//  ProductTestApp
//
//  Created by Nikita Omelchenko on 6/5/19.
//  Copyright © 2019 Nikita Omelchenko. All rights reserved.
//

import UIKit
import Alamofire

struct ApiManager {
    static var sessionManager = Alamofire.SessionManager()
    static var userToken: String!
    static var header: HTTPHeaders {
        get {
            guard userToken != nil else { return HTTPHeaders() }
            return ["Authorization" : "Token \(ApiManager.userToken!)"]
        }
    }
    
    static func errorProcessing(with response: HTTPURLResponse?, error: Error?) {
        UIAlertController.alert(title: String(describing: "Error \(response?.statusCode ?? 0)"), msg: (error?.localizedDescription)!)
        UIApplication.popToRoot()
    }
    
    static func errorProcessing(withMsg msg: String) {
        UIAlertController.alert(title: String(describing: "Error"), msg: msg)
    }
}
