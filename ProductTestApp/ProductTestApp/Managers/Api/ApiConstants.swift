//
//  ApiConstants.swift
//  ProductTestApp
//
//  Created by Nikita Omelchenko on 6/6/19.
//  Copyright © 2019 Nikita Omelchenko. All rights reserved.
//

import UIKit

enum BaseUrl: String {
    case datahost  = "http://smktesting.herokuapp.com/"
    case imageHost = "https://smktesting.herokuapp.com/static/"
}

enum RequestURL: String {
    case registrationUser   = "api/register/"
    case login              = "api/login/"
    case getProductList     = "api/products/"
    case review             = "api/reviews/"
}

